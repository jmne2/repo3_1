package jmn.services;

import java.util.List;

import jmn.models.Banka;

public interface BankaService {
	List<Banka> findAll();
	Banka saveBanka(Banka bnk);
	Banka findOne(Long id);
	void delete(Long id);
	void delete(Banka bnk);
	List<Banka> findByNazivContaining(String naziv);
	

}
