package jmn.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.DnevnoStanje;
import jmn.models.Racun;
import jmn.repositories.DnevnoStanjeRepository;

@Service
public class DnevnoStanjeServiceImpl implements DnevnoStanjeService{

	@Autowired
	private DnevnoStanjeRepository dsr;
	
	@Override
	public List<DnevnoStanje> findAll() {
		return (List<DnevnoStanje>) dsr.findAll();
	}

	@Override
	public DnevnoStanje saveDnevnoStanje(DnevnoStanje dns) {
		return dsr.save(dns);
	}

	@Override
	public DnevnoStanje findOne(Long id) {
		return dsr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		dsr.delete(id);
	}

	@Override
	public void delete(DnevnoStanje dns) {
		dsr.delete(dns);
	}

	@Override
	public DnevnoStanje findByDatumIzvodaAndRacun(Date datumIzvoda, Racun racun) {
		return dsr.findByDatumIzvodaAndRacun(datumIzvoda, racun);
	}

	@Override
	public List<DnevnoStanje> findByDatumIzvoda(Date datumIzvoda) {
		return dsr.findByDatumIzvoda(datumIzvoda);
	}

	@Override
	public List<DnevnoStanje> findByRacun(Racun racun) {
		return dsr.findByRacun(racun);
	}

}
