package jmn.services;

import java.util.List;

import jmn.models.Adresa;
import jmn.models.NaseljenoMesto;

public interface AdresaService {

	List<Adresa> findAll();

	Adresa saveAdresa(Adresa adr);

	Adresa findOne(Long id);

	void delete(Long id);

	void delete(Adresa adr);

	List<Adresa> findByUlicaContainingAndBrojAndNasMesto(String ulica, int broj, NaseljenoMesto nasMesto);

	List<Adresa> findByUlicaContainingAndNasMesto(String ulica, NaseljenoMesto nasMesto);

	List<Adresa> findByUlicaContainingAndBroj(String ulica, int broj);
}
