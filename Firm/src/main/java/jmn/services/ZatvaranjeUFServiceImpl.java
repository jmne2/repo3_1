package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Faktura;
import jmn.models.ZatvaranjeUF;
import jmn.repositories.ZatvaranjeUFRepository;

@Service
public class ZatvaranjeUFServiceImpl implements ZatvaranjeUFService{

	@Autowired
	private ZatvaranjeUFRepository zr;
	
	@Override
	public List<ZatvaranjeUF> findAll() {
		return (List<ZatvaranjeUF>)zr.findAll();
	}

	@Override
	public ZatvaranjeUF saveZatvaranjeUF(ZatvaranjeUF z) {
		return zr.save(z);
	}

	@Override
	public ZatvaranjeUF findOne(Long id) {
		return zr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		zr.delete(id);
	}

	@Override
	public void delete(ZatvaranjeUF z) {
		zr.delete(z);
	}

	@Override
	public List<ZatvaranjeUF> findByUlaznaFaktura(Faktura ulaznaFaktura) {
		return zr.findByUlaznaFaktura(ulaznaFaktura);
	}

}
