package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Banka;
import jmn.repositories.BankaRepository;

@Service
public class BankaServiceImpl implements BankaService{

	@Autowired
	private BankaRepository br;
	
	@Override
	public List<Banka> findAll() {
		return (List<Banka>)br.findAll();
	}

	@Override
	public Banka saveBanka(Banka bnk) {
		return br.save(bnk);
	}

	@Override
	public Banka findOne(Long id) {
		return br.findOne(id);
	}

	@Override
	public void delete(Long id) {
		br.delete(id);
	}

	@Override
	public void delete(Banka bnk) {
		br.delete(bnk);
	}

	@Override
	public List<Banka> findByNazivContaining(String naziv) {
		return br.findByNazivContaining(naziv);
	}

}
