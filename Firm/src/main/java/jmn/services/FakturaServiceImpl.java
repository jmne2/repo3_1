package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Faktura;
import jmn.models.PoslovnaGodina;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.repositories.FakturaRepository;

@Service
public class FakturaServiceImpl implements FakturaService{

	@Autowired
	private FakturaRepository izfr;
	
	@Override
	public List<Faktura> findAll() {
		return (List<Faktura>) izfr.findAll();
	}

	@Override
	public Faktura saveFaktura(Faktura izf) {
		return izfr.save(izf);
	}

	@Override
	public Faktura findOne(Long id) {
		return izfr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		izfr.delete(id);
	}

	@Override
	public void delete(Faktura izf) {
		izfr.delete(izf);
	}

	@Override
	public List<Faktura> findByBrojFaktureAndPoslovnaGodinaAndPoslovniPartner(Long brojFakture,
			PoslovnaGodina poslovnaGodina, PoslovniPartner poslovniPartner) {
		return izfr.findByBrojFaktureAndPoslovnaGodinaAndPoslovniPartner(brojFakture, poslovnaGodina, poslovniPartner);
	}

	@Override
	public List<Faktura> findByBrojFaktureAndPoslovnaGodina(Long brojFakture, PoslovnaGodina poslovnaGodina) {
		return izfr.findByBrojFaktureAndPoslovnaGodina(brojFakture, poslovnaGodina);
	}

	@Override
	public List<Faktura> findByBrojFaktureAndPoslovniPartner(Long brojFakture, PoslovniPartner poslovniPartner) {
		return izfr.findByBrojFaktureAndPoslovniPartner(brojFakture, poslovniPartner);
	}

	@Override
	public List<Faktura> findByVrstaFaktureAndPreduzece(String vrstaFakture, Preduzece preduzece) {
		return izfr.findByVrstaFaktureAndPreduzece(vrstaFakture, preduzece);
	}

	@Override
	public List<Faktura> findByPreduzeceAndBrojFaktureAndVrstaFakture(Preduzece preduzece, String brojFakture,
			String vrstaFakture) {
		return izfr.findByPreduzeceAndBrojFaktureAndVrstaFakture(preduzece, brojFakture, vrstaFakture);
	}

	@Override
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(Preduzece preduzece,String vrstaFakture, double preostaliIznos) {
		return izfr.findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(preduzece,vrstaFakture, preostaliIznos);
	}

	@Override
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(
			Preduzece preduzece, String vrstaFakture, PoslovniPartner poslovniPartner, double preostaliIznos) {
		return izfr.findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(preduzece, vrstaFakture, poslovniPartner, preostaliIznos);
	}

}
