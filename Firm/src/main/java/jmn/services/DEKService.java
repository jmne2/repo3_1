package jmn.services;

import jmn.models.DEK;
import jmn.models.Preduzece;

public interface DEKService {

	DEK saveDEK(DEK dk);
	DEK findOne(Long id);
	void delete(Long id);
	DEK findByPreduzece(Preduzece preduzece);
}
