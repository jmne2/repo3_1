package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Drzava;
import jmn.repositories.DrzavaRepository;

@Service
public class DrzavaServiceImpl implements DrzavaService{

	@Autowired
	private DrzavaRepository drzRepo;
	
	@Override
	public List<Drzava> findAll() {
		return (List<Drzava>) drzRepo.findAll();
	}

	@Override
	public Drzava saveDrzava(Drzava drz) {
		return drzRepo.save(drz);
	}

	@Override
	public Drzava findOne(Long id) {
		return drzRepo.findOne(id);
	}

	@Override
	public void delete(Long id) {
		drzRepo.delete(id);
	}

	@Override
	public void delete(Drzava drz) {
		drzRepo.delete(drz);
	}

	@Override
	public List<Drzava> findByOznakaAndNaziv(String oznaka, String naziv) {
		return drzRepo.findByOznakaContainingAndNazivContaining(oznaka, naziv);
	}

}
