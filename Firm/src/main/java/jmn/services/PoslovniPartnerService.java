package jmn.services;

import java.util.List;

import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;

public interface PoslovniPartnerService {

	List<PoslovniPartner> findAll();

	PoslovniPartner savePoslovniPartner(PoslovniPartner pp);

	PoslovniPartner findOne(Long id);

	void delete(Long id);

	void delete(PoslovniPartner pp);

	List<PoslovniPartner> findByNazivAndPibAndPreduzece(String naziv, int pib, Preduzece preduzece);

	List<PoslovniPartner> findByNazivAndPreduzece(String naziv, Preduzece preduzece);
	
	List<PoslovniPartner> findByPreduzece(Preduzece preduzece);
	
	List<PoslovniPartner> findByPreduzeceAndVrsta(Preduzece preduzece, String vrsta);
	
	PoslovniPartner findByPib(int pib);
	
	PoslovniPartner findByMaticniBroj(Long maticniBroj);
}
