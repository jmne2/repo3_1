package jmn.services;

import java.util.List;

import jmn.models.Drzava;
import jmn.models.NaseljenoMesto;

public interface NaseljenoMestoService {

	List<NaseljenoMesto> findAll();
	NaseljenoMesto save(NaseljenoMesto nas);
	NaseljenoMesto findOne(Long id);
	void delete(Long id);
	void delete(NaseljenoMesto nas);
	List<NaseljenoMesto> findByNazivContainingAndPostBrojContainingAndDrzava(String naziv, String postBroj, Drzava drzava);
	List<NaseljenoMesto> findByNazivContainingAndPostBrojContaining(String naziv, String postBroj);
	
}
