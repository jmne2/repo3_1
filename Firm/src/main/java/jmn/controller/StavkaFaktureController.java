package jmn.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.Faktura;
import jmn.models.StavkaFakture;
import jmn.services.StavkaFaktureService;

@Controller
public class StavkaFaktureController {

	@Autowired
	private StavkaFaktureService sfs;
	
	@PermissionType("StavkeFakture:view")
	@RequestMapping(value="/stavkeFakture/", method=RequestMethod.POST)
	public ResponseEntity<List<StavkaFakture>> listAllStavkeFakture(HttpSession session, @RequestBody Faktura faktura){
		System.out.println("USAOOOOOO SAMMMM U KONTROLERRRRRRRRRRRRR");
		List<StavkaFakture> stavkeFakture = sfs.findByStavkaFakture(faktura);
			
		if(stavkeFakture == null)	
			return new ResponseEntity<List<StavkaFakture>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<StavkaFakture>>(stavkeFakture, HttpStatus.OK);
	}
	
	
	 
	@PermissionType("StavkeFakture:search")
	@RequestMapping(value = "/stavkeFakture/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchFakture(@RequestBody StavkaFakture stavkaFakture, UriComponentsBuilder ucBuilder) {
		 
		 List<StavkaFakture> stavkeFakture = null; 
		 if(stavkeFakture==null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
	     return new ResponseEntity<List<StavkaFakture>>(stavkeFakture, HttpStatus.OK);
    }
	
}
