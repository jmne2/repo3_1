package jmn.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jmn.annotation.PermissionType;
import jmn.configuration.AES;
import jmn.models.DEK;
import jmn.models.Faktura;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.models.SifrarnikMetoda;
import jmn.pojo.UserData;
import jmn.services.DEKService;
import jmn.services.FakturaService;
import jmn.services.PoslovniPartnerService;
import jmn.services.PreduzeceService;;

@Controller
public class FaktureController {

	final static Logger logger = Logger.getLogger(FaktureController.class);
	
	@Autowired
	private FakturaService ufs;
	
	@Autowired
	private PreduzeceService ps;
	
	@Autowired
	private PoslovniPartnerService pps;
	
	@Autowired
	private DEKService dks;
	
	@PermissionType("Fakture:view")
	@RequestMapping(value="/fakture/ulazne/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> fetchUlazneFakture(HttpSession session){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		UserData u = (UserData)session.getAttribute("user");
		Preduzece preduzece = u.getPreduzece();
		int pib = preduzece.getPib();
		System.out.println("Preduzece je" + preduzece.getId());
		/*DEK kk = dks.findByPreduzece(u.getPreduzece());
		kk.setValue(AES.decryptDEK(kk.getValue(), Integer.toString(pib)));*/
		List<PoslovniPartner> partneri = pps.findByPreduzece(preduzece);
		List<Faktura> ulazneFakture = ufs.findByVrstaFaktureAndPreduzece("ulazne", preduzece);
		resp.put("partneri", partneri);
		if(ulazneFakture == null)
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		/*for(Faktura f : ulazneFakture){
			f.getRacunPartnera().setBrRacuna(AES.decryptCBC(f.getRacunPartnera().getBrRacuna(), kk.getValue(), kk.getIv()));
		}*/
		resp.put("ulazneFakture", ulazneFakture);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	
	@PermissionType("Fakture:view")
	@RequestMapping(value="/fakture/izlazne/", method=RequestMethod.GET)
	public ResponseEntity<List<Faktura>> fetchIzlazneFakture(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");

		Preduzece preduzece = u.getPreduzece(); 
		
		List<Faktura> izlazneFakture = ufs.findByVrstaFaktureAndPreduzece("izlazne", preduzece);
		
		if(izlazneFakture == null)
			return new ResponseEntity<List<Faktura>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Faktura>>(izlazneFakture, HttpStatus.OK);
	}
	
	
	@PermissionType("Fakture:edit")
	 @RequestMapping(value = "/fakture/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateUlaznaFaktura(HttpSession session,@PathVariable("id") long id, @RequestBody Faktura ulaznaFaktura) {
			UserData u = (UserData)session.getAttribute("user");
			Faktura exists = ufs.findOne(id);
		 	if(exists == null){
		 		logger.error("Greska pri izvrsenju " + SifrarnikMetoda.methods.get(Thread.currentThread().getStackTrace()[1].getMethodName())
	        	+ " za korisnika " + u.getId());
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
	        logger.warn("Korisnik "+ u.getId() + " uspesno je izvrsio metodu " + SifrarnikMetoda.methods.get(
		 				Thread.currentThread().getStackTrace()[1].getMethodName()));

		 	return new ResponseEntity<Faktura>(exists, HttpStatus.OK);
	    }
	
	
	@PermissionType("Fakture:search")
	@RequestMapping(value = "/fakture/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchUlaznaFaktura(HttpSession session,@RequestBody Faktura ulaznaFaktura) {
			List<Faktura> fakture = null;
			UserData u = (UserData)session.getAttribute("user");
			Preduzece preduzece = u.getPreduzece(); 
			fakture = ufs.findByPreduzeceAndBrojFaktureAndVrstaFakture(preduzece, ulaznaFaktura.getBrojFakture().toString(), ulaznaFaktura.getVrstaFakture());
			if(fakture == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	     
			return new ResponseEntity<List<Faktura>>(fakture, HttpStatus.OK);
	    }	
}
