package jmn.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "stavka")
public class StavkaFaktureRS {

	private Long redniBroj;
	
	private String nazivRobe;
	
	private double kolicina;
	
	private String jedinicaMere;
	
	private double jedinicnaCena;
	
	private double vrednost;
	
	private double procenaRabata;
	
	private double iznostRabata;
	
	private double umanjenoZaRabat;
	
	private double ukupanPorez;

	public StavkaFaktureRS() {
		super();
	}

	public Long getRedniBroj() {
		return redniBroj;
	}

	public void setRedniBroj(Long redniBroj) {
		this.redniBroj = redniBroj;
	}

	public String getNazivRobe() {
		return nazivRobe;
	}

	public void setNazivRobe(String nazivRobe) {
		this.nazivRobe = nazivRobe;
	}

	public double getKolicina() {
		return kolicina;
	}

	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}

	public String getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(String jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

	public double getJedinicnaCena() {
		return jedinicnaCena;
	}

	public void setJedinicnaCena(double jedinicnaCena) {
		this.jedinicnaCena = jedinicnaCena;
	}

	public double getVrednost() {
		return vrednost;
	}

	public void setVrednost(double vrednost) {
		this.vrednost = vrednost;
	}

	public double getProcenaRabata() {
		return procenaRabata;
	}

	public void setProcenaRabata(double procenaRabata) {
		this.procenaRabata = procenaRabata;
	}

	public double getIznostRabata() {
		return iznostRabata;
	}

	public void setIznostRabata(double iznostRabata) {
		this.iznostRabata = iznostRabata;
	}

	public double getUmanjenoZaRabat() {
		return umanjenoZaRabat;
	}

	public void setUmanjenoZaRabat(double umanjenoZaRabat) {
		this.umanjenoZaRabat = umanjenoZaRabat;
	}

	public double getUkupanPorez() {
		return ukupanPorez;
	}

	public void setUkupanPorez(double ukupanPorez) {
		this.ukupanPorez = ukupanPorez;
	}
	
	
	
}
