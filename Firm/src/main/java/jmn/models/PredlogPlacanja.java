package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PredlogPlacanja implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private Date datum;
	
	@Column(nullable = false)
	private String broj;
	
	@Column(nullable = false)
	private String status;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="predlogPlacanja")
	@JsonIgnore
	private Set<StaSePlaca> listaStaSePlaca = new HashSet<StaSePlaca>();

	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="zaPredlog")
	@JsonIgnore
	private Set<NalogZaPlacanje> naloziZaPlacanje = new HashSet<NalogZaPlacanje>();
	
	@ManyToOne()
	@JoinColumn(name="preduzece")
	private Preduzece preduzece;
	
	public PredlogPlacanja() {
	}

	public PredlogPlacanja(Date datum, String broj, String status, Set<StaSePlaca> listaStaSePlaca,
			Set<NalogZaPlacanje> naloziZaPlacanje, Preduzece preduzece) {
		super();
		this.datum = datum;
		this.broj = broj;
		this.status = status;
		this.listaStaSePlaca = listaStaSePlaca;
		this.naloziZaPlacanje = naloziZaPlacanje;
		this.preduzece = preduzece;
	}


	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<StaSePlaca> getListaStaSePlaca() {
		return listaStaSePlaca;
	}

	public void setListaStaSePlaca(Set<StaSePlaca> listaStaSePlaca) {
		this.listaStaSePlaca = listaStaSePlaca;
	}

	public void addStaSePlaca(StaSePlaca ssp){
		this.listaStaSePlaca.add(ssp);
		if(!ssp.getPredlogPlacanja().equals(this))
			ssp.setPredlogPlacanja(this);
	}
	
	public Long getId() {
		return id;
	}

	public Set<NalogZaPlacanje> getNaloziZaPlacanje() {
		return naloziZaPlacanje;
	}

	public void setNaloziZaPlacanje(Set<NalogZaPlacanje> naloziZaPlacanje) {
		this.naloziZaPlacanje = naloziZaPlacanje;
	}
	
	public void addNalogZaPlacanje(NalogZaPlacanje nzp){
		this.naloziZaPlacanje.add(nzp);
		if(!nzp.getZaPredlog().equals(this))
			nzp.setZaPredlog(this);
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
	}
	
}
