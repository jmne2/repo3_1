package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Faktura {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column (nullable=false)
	private Long brojFakture;
	
	@Column (nullable=false)
	private Date datumFakture;
	
	@Column (nullable=false)
	private Date datumValute;
	
	@Column (nullable=true)
	private double vrednostRobe;
	
	@Column (nullable=true)
	private double vrednostUsluga;
	
	@Column (nullable=true)
	private double ukupnoRobaIUsluge;
	
	@Column (nullable=true)
	private double ukupanRabat;
	
	@Column (nullable=false)
	private double ukupanPorez;
	
	@Column (nullable=false)
	private String oznakaValute;
	
	@Column (nullable=false)
	private double ukIznos;
		
	@Column (nullable=false)
	private double preostaliIznos;
	
	@Column (nullable=false)
	private String vrstaFakture;
	
	@ManyToOne()
	@JoinColumn(name="poslovnaGodina")
	private PoslovnaGodina poslovnaGodina;
	
	@ManyToOne()
	@JoinColumn(name="preduzece")
	private Preduzece preduzece;
	
	@ManyToOne()
	@JoinColumn(name="poslovniPartner")
	private PoslovniPartner poslovniPartner;
	
	@ManyToOne()
	@JoinColumn(name="racunPartnera")
	private RacunPartnera racunPartnera;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="izlaznaFaktura")
	@JsonIgnore
	private Set<ZatvaranjeIF> listaZatvaranjaIF = new HashSet<ZatvaranjeIF>();

	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="ulaznaFaktura")
	@JsonIgnore
	private Set<ZatvaranjeUF> listaZatvaranjaUF = new HashSet<ZatvaranjeUF>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="stavkaFakture")
	@JsonIgnore
	private Set<StavkaFakture> listaStavkiFakture = new HashSet<StavkaFakture>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="faktura")
	@JsonIgnore
	private Set<StaSePlaca> listaStaSePlaca = new HashSet<StaSePlaca>();

	//ubaciti racun partnera
	
	public Faktura() {
	}

	

	public Faktura(Long brojFakture, Date datumFakture, Date datumValute, double vrednostRobe, double vrednostUsluga,
			double ukupnoRobaIUsluge, double ukupanRabat, double ukupanPorez, String oznakaValute, double ukIznos,
			double preostaliIznos, String vrstaFakture, PoslovnaGodina poslovnaGodina, Preduzece preduzece,
			PoslovniPartner poslovniPartner, RacunPartnera racunPartnera, Set<ZatvaranjeIF> listaZatvaranjaIF,
			Set<ZatvaranjeUF> listaZatvaranjaUF, Set<StavkaFakture> listaStavkiFakture,
			Set<StaSePlaca> listaStaSePlaca) {
		super();
		this.brojFakture = brojFakture;
		this.datumFakture = datumFakture;
		this.datumValute = datumValute;
		this.vrednostRobe = vrednostRobe;
		this.vrednostUsluga = vrednostUsluga;
		this.ukupnoRobaIUsluge = ukupnoRobaIUsluge;
		this.ukupanRabat = ukupanRabat;
		this.ukupanPorez = ukupanPorez;
		this.oznakaValute = oznakaValute;
		this.ukIznos = ukIznos;
		this.preostaliIznos = preostaliIznos;
		this.vrstaFakture = vrstaFakture;
		this.poslovnaGodina = poslovnaGodina;
		this.preduzece = preduzece;
		this.poslovniPartner = poslovniPartner;
		this.racunPartnera = racunPartnera;
		this.listaZatvaranjaIF = listaZatvaranjaIF;
		this.listaZatvaranjaUF = listaZatvaranjaUF;
		this.listaStavkiFakture = listaStavkiFakture;
		this.listaStaSePlaca = listaStaSePlaca;
	}



	public Long getBrojFakture() {
		return brojFakture;
	}

	public void setBrojFakture(Long brojFakture) {
		this.brojFakture = brojFakture;
	}

	public Date getDatumFakture() {
		return datumFakture;
	}

	public void setDatumFakture(Date datumFakture) {
		this.datumFakture = datumFakture;
	}

	public Date getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}

	public double getVrednostRobe() {
		return vrednostRobe;
	}

	public void setVrednostRobe(double vrednostRobe) {
		this.vrednostRobe = vrednostRobe;
	}

	public double getVrednostUsluga() {
		return vrednostUsluga;
	}

	public void setVrednostUsluga(double vrednostUsluga) {
		this.vrednostUsluga = vrednostUsluga;
	}

	public double getUkupnoRobaIUsluge() {
		return ukupnoRobaIUsluge;
	}

	public void setUkupnoRobaIUsluge(double ukupnoRobaIUsluge) {
		this.ukupnoRobaIUsluge = ukupnoRobaIUsluge;
	}

	public double getUkupanRabat() {
		return ukupanRabat;
	}

	public void setUkupanRabat(double ukupanRabat) {
		this.ukupanRabat = ukupanRabat;
	}

	public double getUkupanPorez() {
		return ukupanPorez;
	}

	public void setUkupanPorez(double ukupanPorez) {
		this.ukupanPorez = ukupanPorez;
	}

	public String getOznakaValute() {
		return oznakaValute;
	}

	public void setOznakaValute(String oznakaValute) {
		this.oznakaValute = oznakaValute;
	}

	public double getUkIznos() {
		return ukIznos;
	}

	public void setUkIznos(double ukIznos) {
		this.ukIznos = ukIznos;
	}

	public double getPreostaliIznos() {
		return preostaliIznos;
	}

	public void setPreostaliIznos(double preostaliIznos) {
		this.preostaliIznos = preostaliIznos;
	}

	public String getVrstaFakture() {
		return vrstaFakture;
	}

	public void setVrstaFakture(String vrstaFakture) {
		this.vrstaFakture = vrstaFakture;
	}

	public PoslovnaGodina getPoslovnaGodina() {
		return poslovnaGodina;
	}

	public void setPoslovnaGodina(PoslovnaGodina poslovnaGodina) {
		this.poslovnaGodina = poslovnaGodina;
		if(poslovnaGodina.getFakture()==null || !poslovnaGodina.getFakture().contains(this)){
			poslovnaGodina.addFaktura(this);
		}
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
		if(preduzece.getFakture()==null || !preduzece.getFakture().contains(this)){
			preduzece.addFaktura(this);
		}
	}

	public PoslovniPartner getPoslovniPartner() {
		return poslovniPartner;
	}

	public void setPoslovniPartner(PoslovniPartner poslovniPartner) {
		this.poslovniPartner = poslovniPartner;
		if(poslovniPartner.getFakture()==null || !poslovniPartner.getFakture().contains(this)){
			poslovniPartner.addFaktura(this);
		}
	}

	public Set<ZatvaranjeIF> getListaZatvaranjaIF() {
		return listaZatvaranjaIF;
	}

	public void setListaZatvaranjaIF(Set<ZatvaranjeIF> listaZatvaranjaIF) {
		this.listaZatvaranjaIF = listaZatvaranjaIF;
	}

	public Set<ZatvaranjeUF> getListaZatvaranjaUF() {
		return listaZatvaranjaUF;
	}

	public void setListaZatvaranjaUF(Set<ZatvaranjeUF> listaZatvaranjaUF) {
		this.listaZatvaranjaUF = listaZatvaranjaUF;
	}

	public Set<StavkaFakture> getListaStavkiFakture() {
		return listaStavkiFakture;
	}

	public void setListaStavkiFakture(Set<StavkaFakture> listaStavkiFakture) {
		this.listaStavkiFakture = listaStavkiFakture;
	}

	public Set<StaSePlaca> getListaStaSePlaca() {
		return listaStaSePlaca;
	}

	public void setListaStaSePlaca(Set<StaSePlaca> listaStaSePlaca) {
		this.listaStaSePlaca = listaStaSePlaca;
	}

	public Long getId() {
		return id;
	}
	
	public void addZatvaranjeIF(ZatvaranjeIF zatv){
		this.listaZatvaranjaIF.add(zatv);
		if(!zatv.getFaktura().equals(this))
			zatv.setFaktura(this);
	}
	
	public void addZatvaranjeUF(ZatvaranjeUF zatv){
		this.listaZatvaranjaUF.add(zatv);
		if(!zatv.getFaktura().equals(this))
			zatv.setFaktura(this);
	}
	
	public void addStavkuFakture(StavkaFakture stF){
		this.listaStavkiFakture.add(stF);
		if(!stF.getStavkaFakture().equals(this))
			stF.setStavkaFakture(this);
	}
	
	public void addStaSePlaca(StaSePlaca ssp){
		this.listaStaSePlaca.add(ssp);
		if(!ssp.getFaktura().equals(this))
			ssp.setUlaznaFaktura(this);
	}

	public RacunPartnera getRacunPartnera() {
		return racunPartnera;
	}

	public void setRacunPartnera(RacunPartnera racunPartnera) {
		this.racunPartnera = racunPartnera;
		if(racunPartnera.getListaFaktura()==null || !racunPartnera.getListaFaktura().contains(this)){
			racunPartnera.addFaktura(this);
		}
	}
	
}
