package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class RacunPartnera implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String brRacuna;
	
	@ManyToOne()
	@JoinColumn(name="poslovniPartner")
	private PoslovniPartner poslovniPartner;
	
	@ManyToOne()
	@JoinColumn(name="banka")
	private Banka banka;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="racunPartnera")
	@JsonIgnore
	private Set<Faktura> listaFaktura = new HashSet<Faktura>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="racunPrimaoca")
	@JsonIgnore
	private Set<NalogZaPlacanje> nalozi = new HashSet<NalogZaPlacanje>();

	public RacunPartnera() {
	}

	public RacunPartnera(String brRacuna, PoslovniPartner poslovniPartner, Banka banka, Set<Faktura> listaFaktura, Set<NalogZaPlacanje> nalozi) {
		super();
		this.brRacuna = brRacuna;
		this.poslovniPartner = poslovniPartner;
		this.banka = banka;
		this.listaFaktura = listaFaktura;
		this.nalozi = nalozi;
	}



	public String getBrRacuna() {
		return brRacuna;
	}

	public void setBrRacuna(String brRacuna) {
		this.brRacuna = brRacuna;
	}

	public PoslovniPartner getPoslovniPartner() {
		return poslovniPartner;
	}

	public void setPoslovniPartner(PoslovniPartner poslovniPartner) {
		this.poslovniPartner = poslovniPartner;
	}

	public Banka getBanka() {
		return banka;
	}

	public void setBanka(Banka banka) {
		this.banka = banka;
	}

	public Long getId() {
		return id;
	}
	
	public void addFaktura(Faktura f){
		this.listaFaktura.add(f);
		if(!f.getRacunPartnera().equals(this))
			f.setRacunPartnera(this);
	}

	public Set<Faktura> getListaFaktura() {
		return listaFaktura;
	}

	public void setListaFaktura(Set<Faktura> listaFaktura) {
		this.listaFaktura = listaFaktura;
	}

	public Set<NalogZaPlacanje> getNalozi() {
		return nalozi;
	}

	public void setNalozi(Set<NalogZaPlacanje> nalozi) {
		this.nalozi = nalozi;
	}
	
	public void addNalog(NalogZaPlacanje f){
		this.nalozi.add(f);
		if(!f.getRacunPrimaoca().equals(this))
			f.setRacunPrimaoca(this);
	}
}
