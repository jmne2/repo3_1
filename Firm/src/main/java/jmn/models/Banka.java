package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Banka implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String naziv;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="banka")
	@JsonIgnore
	private Set<Racun> racuni = new HashSet<Racun>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="banka")
	@JsonIgnore
	private Set<RacunPartnera> racuniPartnera = new HashSet<RacunPartnera>();
	
	public Banka() {
	}

	public Banka(String naziv, Set<Racun> racuni, Set<RacunPartnera> racuniPartnera) {
		super();
		this.naziv = naziv;
		this.racuni = racuni;
		this.racuniPartnera = racuniPartnera;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<Racun> getRacuni() {
		return racuni;
	}

	public void setRacuni(Set<Racun> racuni) {
		this.racuni = racuni;
	}

	public Set<RacunPartnera> getRacuniPartnera() {
		return racuniPartnera;
	}

	public void setRacuniPartnera(Set<RacunPartnera> racuniPartnera) {
		this.racuniPartnera = racuniPartnera;
	}

	public Long getId() {
		return id;
	}
	
	public void addRacun(Racun r){
		this.racuni.add(r);
		if(!r.getBanka().equals(this))
			r.setBanka(this);
	}
	
	public void addRacunPartnera(RacunPartnera rp){
		this.racuniPartnera.add(rp);
		if(!rp.getBanka().equals(this))
			rp.setBanka(this);
	} 
}
