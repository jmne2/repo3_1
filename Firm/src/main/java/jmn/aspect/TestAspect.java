package jmn.aspect;

import java.lang.reflect.Method;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import jmn.annotation.PermissionType;
import jmn.controller.IndexController;
import jmn.models.SifrarnikMetoda;
import jmn.pojo.UserData;
import jmn.pojo.UserHolder;
import jmn.services.RoleService;
import jmn.services.UserService;

@Aspect
//Mora @Component anotacija inace nece ucitati askept klasu
@Component
public class TestAspect {
	
	final static Logger logger = Logger.getLogger(TestAspect.class);
	
	//izvrsice se pre metode navedene
	//mora puna putanja do metode
	//ovo drugo je da moze da namapira argumente da ne bi preko jointPoint.getArgs() trazili argumente
	/*@Before("execution(* jmn.controller.IndexController.login(..)) && args(uh,..)")
	public void testPrint(JoinPoint joinPoint, UserHolder uh){
		System.out.println("testPrint() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature().getName());
        System.out.println("hijacked arguments are : " + uh.getUname() + " and " + uh.getPname());
        System.out.println("******");
	}*/
	
	@Around("execution(* jmn.controller.IndexController.login(..)) && args(uh, session,..)")
	public Object permissionValidator(ProceedingJoinPoint jp, UserHolder uh, HttpSession session){
		MethodSignature signature = (MethodSignature) jp.getSignature();
	    Method method = signature.getMethod();
	    System.out.println("hijacked : " + jp.getSignature().getName());
	    ResponseEntity<UserData> retVal = null;
	    try {
			retVal = (ResponseEntity<UserData>)jp.proceed(); 
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			logger.error("Neuspesan pristup " + SifrarnikMetoda.methods.get(jp.getSignature().getName()));
			e.printStackTrace();
		}
	    return retVal;
	}
}
