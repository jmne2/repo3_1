package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.SifrarnikDelatnosti;

public interface SifrarnikDelatnostiRepository extends CrudRepository<SifrarnikDelatnosti, Long> {
	public List<SifrarnikDelatnosti> findByNazivDelatnostiContainingAndSifraContaining(String nazivDelatnosti, String sifra);

}
