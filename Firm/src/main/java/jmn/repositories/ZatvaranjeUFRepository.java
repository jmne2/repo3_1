package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Faktura;
import jmn.models.ZatvaranjeUF;

public interface ZatvaranjeUFRepository extends CrudRepository<ZatvaranjeUF, Long>{

	public List<ZatvaranjeUF> findByUlaznaFaktura(Faktura ulaznaFaktura);
}
