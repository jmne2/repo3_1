package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Adresa;
import jmn.models.NaseljenoMesto;

public interface AdresaRepository extends CrudRepository<Adresa, Long>{

	public List<Adresa> findByUlicaContainingAndBrojAndNasMesto(String ulica, int broj, NaseljenoMesto nasMesto);
	public List<Adresa> findByUlicaContainingAndNasMesto(String ulica, NaseljenoMesto nasMesto);
	public List<Adresa> findByUlicaContainingAndBroj(String ulica, int broj);
}
