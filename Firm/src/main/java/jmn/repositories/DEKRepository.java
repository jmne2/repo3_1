package jmn.repositories;

import org.springframework.data.repository.CrudRepository;

import jmn.models.DEK;
import jmn.models.Preduzece;

public interface DEKRepository extends CrudRepository<DEK, Long>{

	DEK findByPreduzece(Preduzece preduzece);
}
