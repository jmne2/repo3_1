package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Drzava;
import jmn.models.NaseljenoMesto;

public interface NaseljenoMestoRepository extends CrudRepository<NaseljenoMesto, Long>{

	public void delete(NaseljenoMesto nas);
	public List<NaseljenoMesto> findByNazivContainingAndPostBrojContainingAndDrzava(String naziv, String postBroj, Drzava drzava);
	public List<NaseljenoMesto> findByNazivContainingAndPostBrojContaining(String naziv, String postBroj);
}
