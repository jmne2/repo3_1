package jmn.repositories;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Permission;

public interface PermissionRepository extends CrudRepository<Permission, Long>{

}
