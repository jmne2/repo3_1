'use strict';
 
angular.module('repo3App').factory('PlacanjeService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    var factory = {
        fetchAllPlacanje: fetchAllPlacanje,
        createPlacanje: createPlacanje,
        updatePlacanje:updatePlacanje,
        deletePlacanje:deletePlacanje,
        searchPlacanje: searchPlacanje
    };
 
    return factory;
 
    function fetchAllPlacanje() {
        var deferred = $q.defer();
        $http.get(urls.PLACA_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Reciepts');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function createPlacanje(placanje) {
        var deferred = $q.defer();
        $http.post(urls.PLACA_SERVICE, placanje)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updatePlacanje(placanje, id) {
        var deferred = $q.defer();
        $http.post(urls.PLACA_SERVICE+id, placanje)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deletePlacanje(id) {
        var deferred = $q.defer();
        $http.delete(urls.PLACA_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function searchPlacanje(placanje){
    	var deferred = $q.defer();
        $http.post(urls.PLACA_SERVICE+"search/", placanje)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);