
'use strict';
 
angular.module('repo3App').factory('PreduzeceService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    
 
    var factory = {
        fetchAllPreduzeca: fetchAllPreduzeca,
        createPreduzece: createPreduzece,
        updatePreduzece:updatePreduzece,
        deletePreduzece:deletePreduzece,
        searchPreduzeca: searchPreduzeca
    };
 
    return factory;
 
    function fetchAllPreduzeca() {
        var deferred = $q.defer();
        $http.get(urls.PREDUZECE_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Partners');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createPreduzece(preduzece) {
        var deferred = $q.defer();
        $http.post(urls.PREDUZECE_SERVICE, preduzece)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Partner');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updatePreduzece(preduzece, id) {
        var deferred = $q.defer();
        $http.post(urls.PREDUZECE_SERVICE+id, preduzece)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deletePreduzece(id) {
        var deferred = $q.defer();
        $http.delete(urls.PREDUZECE_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchPreduzeca(preduzece){
    	var deferred = $q.defer();
        $http.post(urls.PREDUZECE_SERVICE+"search/", preduzece)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);