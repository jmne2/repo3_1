'use strict';
 
angular.module('repo3App').factory('PoslovnaGodinaService', ['$http', '$q', 'urls', function($http, $q, urls){
 
   
 
    var factory = {
        fetchAllGodine: fetchAllGodine,
        createGodinu: createGodinu,
        updateGodinu: updateGodinu,
        deleteGodinu: deleteGodinu,
        searchGodine: searchGodine
    };
 
    return factory;
 
    function fetchAllGodine() {
        var deferred = $q.defer();
        $http.get(urls.GODINE_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Users');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createGodinu(godina) {
        var deferred = $q.defer();
        $http.post(urls.GODINE_SERVICE, godina)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateGodinu(godina, id) {
        var deferred = $q.defer();
        $http.post(urls.GODINE_SERVICE+id, godina)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteGodinu(id) {
        var deferred = $q.defer();
        $http.delete(urls.GODINE_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchGodine(godina){
    	var deferred = $q.defer();
        $http.post(urls.GODINE_SERVICE+"search/", godina)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);