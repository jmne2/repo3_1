'use strict';
 
angular.module('repo3App').factory('StavkeFaktureService', ['$http', '$q', 'urls', function($http, $q, urls){
 
   
 
    var factory = {
    	fetchAllStavkeFakture: fetchAllStavkeFakture,
        searchStavkeFakture: searchStavkeFakture
    };
 
    return factory;
 
    function fetchAllStavkeFakture(faktura) {
        var deferred = $q.defer();
        $http.post(urls.STAVKE_FAKTURE_SERVICE,faktura)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching stavke fakture');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    
    function searchStavkeFakture(stavkaFakture){
    	var deferred = $q.defer();
        $http.post(urls.STAVKE_FAKTURE_SERVICE+"search/", stavkaFakture)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);