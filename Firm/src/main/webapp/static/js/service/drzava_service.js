
'use strict';
 
angular.module('repo3App').factory('DrzavaService', ['$http', '$q', 'urls', function($http, $q, urls){
 
 
    var factory = {
        fetchAllDrzave: fetchAllDrzave,
        createDrzava: createDrzava,
        updateDrzava:updateDrzava,
        deleteDrzava:deleteDrzava,
        searchDrzave: searchDrzave
    };
 
    return factory;
 
    function fetchAllDrzave() {
        var deferred = $q.defer();
        $http.get(urls.DRZAVA_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Users');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createDrzava(drzava) {
        var deferred = $q.defer();
        $http.post(urls.DRZAVA_SERVICE, drzava)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateDrzava(drzava, id) {
        var deferred = $q.defer();
        $http.post(urls.DRZAVA_SERVICE+id, drzava)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteDrzava(id) {
        var deferred = $q.defer();
        $http.delete(urls.DRZAVA_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchDrzave(drzava){
    	var deferred = $q.defer();
        $http.post(urls.DRZAVA_SERVICE+"search/", drzava)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);