
'use strict';
 
angular.module('repo3App').factory('RacunService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    //var SERVICE_URI = 'http://localhost:8080/drzave/';
 
    var factory = {
        fetchAllRacune: fetchAllRacune,
        createRacun: createRacun,
        updateRacun:updateRacun,
        deleteRacun:deleteRacun,
        searchRacune: searchRacune
    };
 
    return factory;
 
    function fetchAllRacune() {
        var deferred = $q.defer();
        $http.get(urls.RACUN_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Reciepts');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createRacun(racun) {
        var deferred = $q.defer();
        $http.post(urls.RACUN_SERVICE, racun)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Reciept');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateRacun(racun, id) {
        var deferred = $q.defer();
        $http.post(urls.RACUN_SERVICE+id, racun)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteRacun(id) {
        var deferred = $q.defer();
        $http.delete(urls.RACUN_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchRacune(racun){
    	var deferred = $q.defer();
        $http.post(urls.RACUN_SERVICE+"search/", racun)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);