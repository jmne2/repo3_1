'use strict';
/*
IZMENI SVE ZA NASELJENO MESTO!!!!!!!!!!!
===============================
TREBA ZOOOM DA NAPRAVIS I TESTIRAS!!!!!!!
====================================
I SEARCH ZA DRZAVE I MESTA!!!!!!!
*/
angular.module('repo3App').controller('NaseljenoMestoController', ['$scope','$uibModal', 'NaseljenoMestoService', function($scope,$uibModal,NaseljenoMestoService) {
    var self = this;
    self.drzava={id:null, oznaka:'', naziv:''};
    self.drzave=[];
    self.mesto = {naziv:'', postBroj:''};
    self.mesta = [];
    self.show = false;
    self.logged = false;
    self.children = ["Adrese"];
    
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.showCountries = showCountries;
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('hidingYoCountries', function(event, args) {
    	fetchAllMesta();
    	self.show = args.arg[1];
    	self.currState = "edit";
    });
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		if(args.parent != null){
    			self.mesto.drzava = args.parent.drzave[args.parent.indSelected];
    			search(self.mesto);
    		} else {
    			fetchAllMesta();
    		}
    		
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.mesto = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllMesta();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selected = self.mesta[$scope.indSelectedVote];
    			remove(selected.id);
    			self.currState = "edit";
    		}
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.mesta.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.mesta.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = $scope.indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.mesta[indSelectedVote].id;
    	   edit(id);
   		}
    };
   
    function showCountries() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.drzave;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.drzava = self.drzave[selectedItem];
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };
    
    function fetchAllMesta(){
    	NaseljenoMestoService.fetchAllMesta()
            .then(
            function(d) {
            	self.mesta = [];
                self.drzave = d["drzave"];
                self.mesta = d["mesta"];
                //$scope.indSelectedVote = 0;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    function createMesto(mesto){
    	NaseljenoMestoService.createMesto(mesto)
            .then(
            fetchAllMesta,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updateMesto(mesto, id){
    	NaseljenoMestoService.updateMesto(mesto, id)
            .then(
            fetchAllMesta,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteMesto(id){
    	NaseljenoMestoService.deleteMesto(id)
            .then(
            fetchAllMesta,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
    	if (self.currState === "add"){
    		if(self.drzava.id != null && !angular.isUndefined(self.drzava.id)){
	            console.log('Saving New THing', self.mesto);
	            self.mesto.drzava = self.drzava;
	            createMesto(self.mesto);
    		}
        }else if (self.currState === "edit"){
        	if(self.drzava.id != null && !angular.isUndefined(self.drzava.id)){
	        	self.mesto.drzava = self.drzava;
	            updateMesto(self.mesto, self.mesto.id);
        	}
        } else if (self.currState === "search"){
        	self.mesto.drzava = self.drzava;
        	search(self.mesto);
        }
	        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.mesta.length; i++){
            if(self.mesta[i].id === id) {
                self.mesto = angular.copy(self.mesta[i]);
                self.drzava = self.mesto.drzava;
                break;
            }
        }
    }
 
    function remove(id){
        console.log('id to be deleted', id);
        if(self.mesto.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteMesto(id);
    }
 
 
    function reset(){
        self.mesto={};
        self.drzava = {};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(mesto){
    	NaseljenoMestoService.searchMesta(mesto)
        .then(
        function(d) {
        	self.mesta = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };

    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};
}]);