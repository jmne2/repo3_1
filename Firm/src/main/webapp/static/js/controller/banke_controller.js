'use strict';
 
angular.module('repo3App').controller('BankaController', BNKCtrl);

function BNKCtrl($scope, BankaService) {
    var self = this;
    self.banka={};
    self.banke=[];
    self.show = false;
    self.logged = false;
 
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.first = first;
    self.prev = prev;
    self.next = next;
    self.last = last;
    self.search = search;
    self.currState = "edit";
    self.children = ["Racuni"];
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.banka = {};
    	}
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllBanke();
    	}
    });
    //TODO: change or scrap!!!
    $scope.$on('hidingYoCountries', function(event, args) {
    	self.show = args.arg[0];
    	
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selectedDrz = self.banke[$scope.indSelectedVote];
    			remove(selectedDrz.id);
    			self.currState = "edit";
    		}
    	}
    });
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		fetchAllBanke();
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    function indexOfRole(propertyName,lookingForValue,array) {
        for (var i in array) {
            if (array[i][propertyName] == lookingForValue) {
                return i;
            }
        }
        return -1;
    }
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.banke.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.banke.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
    
    $scope.setSelected = function (indSelectedVote) {
    	$scope.indSelectedVote = indSelectedVote;
    	self.indSelected = indSelectedVote;
    	if(self.currState === "edit"){
    		var id = self.banke[indSelectedVote].id;
    		edit(id);
    	}
    };
    $scope.changeState = function(state){
    	if(self.currState != state){
    		self.currState = state;
    	}
    }
    
    function fetchAllBanke(){
        BankaService.fetchAllBanke()
            .then(
            function(d) {
            	self.banke = d;
                $scope.indSelectedVote = -1;
                self.indSelected = -1;
                self.show = true;
                if (self.currState === "delete"){
                	self.currState = "edit";
                }
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    function createBanku(banka){
        BankaService.createBanku(banka)
            .then(
            fetchAllBanke,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updateBanku(banka, id){
        BankaService.updateBanku(banka, id)
            .then(
            fetchAllBanke,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteBanku(id){
        BankaService.deleteBanku(id)
            .then(
            fetchAllBanke,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
        
    	if(self.currState === "add"){
            createBanku(self.banka);        	
        } else if(self.currState === "edit"){
            updateBanku(self.banka, self.banka.id);
        } else if (self.currState === "search"){
        	search(self.banka);
        }
        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.banke.length; i++){
            if(self.banke[i].id === id) {
                self.banka = angular.copy(self.banke[i]);
                break;
            }
        }
    }
 
    function remove(id){
        console.log('id to be deleted', id);
        if(self.banka.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteBanku(id);
    }
 
    function search(banka){
    	BankaService.searchBanke(banka)
        .then(
        function(d) {
        	self.banke = d;
            $scope.indSelectedVote = -1;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    function reset(){
        self.banka={};
        $scope.myForm.$setPristine(); //reset Form
    }
};