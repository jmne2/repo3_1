<div class="generic-container" id="RacuniPartnera" ng-controller="RacunPartneraController as ctrl">
          <script type="text/ng-template" id="myModalContent.html">
        	<div class="modal-header">
            	<h3>Izaberite {{type}}</h3>
        	</div>
        	<div class="modal-body">
            	<ul>
                	<li ng-repeat="item in items">
                    	<a ng-click="selected.item = $index">{{ item.naziv }}</a>
                	</li>
            	</ul>
            	Selected: <b>{{ selected.item }} </b>
        	</div>
        		<div class="modal-footer">
            	<button class="btn btn-primary" ng-click="ok()">OK</button>
            	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        	</div>
    	</script>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Racuna Partnera</span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Broj Racuna</th>
                              <th>Banka</th>
                              <th>Preduzece Partnera</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="r in ctrl.racuni" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="r.id"></span></td>
                              <td><span ng-bind="r.brRacuna"></span></td>
                              <td><span ng-bind="r.banka.naziv"></span></td>
                              <td><span ng-bind="r.poslovniPartner.naziv"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.racun.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="brojRacuna">brojRacuna</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.racun.brRacuna" id="brojRacuna" class="naziv form-control input-sm" placeholder="Unesite broj racuna" ng-required="required" ng-minlength="18"
                                  ng-maxlength="18"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.brojRacuna.$error.required">This is a required field</span>
                                      <span ng-show="myForm.brojRacuna.$error.minlength">Minimum length required is 18</span>
                                      <span ng-show="myForm.brojRacuna.$error.maxlength">Minimum length required is 18</span>
                                      <span ng-show="myForm.brojRacuna.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="banka">Banka</label>
                              <button type="button" ng-click="ctrl.showBanks()" class="btn btn-info btn-sm">Izaberi Banku</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="banka">{{ctrl.banka.naziv}}</label>
                              </div>
                          </div>
                      </div>
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="preduzece">Partnera</label>
                               <button type="button" ng-click="ctrl.showCorps()" class="btn btn-info btn-sm">Izaberi Partnera</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="preduzece">{{ctrl.poslovniPartner.naziv}}</label>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                               <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>

   </div>
