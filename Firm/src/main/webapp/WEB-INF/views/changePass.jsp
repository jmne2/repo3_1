<div class="panel panel-default" style="max-width:400px;margin-left:auto; margin-right:auto;">
              <div class="formcontainer">
                  <form ng-submit="changeP()" name="chngForm" class="form-horizontal">
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="pName">Nova Lozinka</label>
                              <div class="col-md-7">
                                  <input type="password" ng-model="us.pname" id="pName" class="naziv form-control input-sm" placeholder="Unesite Lzinku" required ng-minlength="1"/>
                                  <div class="has-error" ng-show="chngForm.$dirty">
                                      <span ng-show="chngForm.pName.$error.required">This is a required field</span>
                                      <span ng-show="chngForm.pName.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="chngForm.pName.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-actions">
                              <input type="submit"  id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="chngForm.$invalid">
                          </div>
                      </div>
                  </form>
              </div>
          </div>
    