<div class="generic-container" ng-controller="BankaController as ctrl" id="Banke" ng-show="ctrl.show">
       <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Banaka </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Naziv</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="b in ctrl.banke" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="b.id"></span></td>
                              <td><span ng-bind="b.naziv"></span></td>
                              
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.banka.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="naziv">Naziv</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.banka.naziv" id="naziv" class="oznaka form-control input-sm" placeholder="Unesite naziv" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.naziv.$error.required">This is a required field</span>
                                      <span ng-show="myForm.naziv.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.naziv.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                                                
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit" id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                              <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
 
      </div>
      </div>
