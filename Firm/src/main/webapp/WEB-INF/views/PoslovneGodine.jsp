<div class="generic-container" ng-controller="PoslovnaGodinaController as ctrl" id="PoslovneGodine">
       <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Poslovnih godina </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Godina</th>
                              <th>Stanje</th>
                              <th>Preduzece</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="g in ctrl.godine" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="g.id"></span></td>
                              <td><span ng-bind="g.godina"></span></td>
                              <td><span ng-if="g.zakljucena">Zakljucena</span> <span ng-if="!g.zakljucena">Otvorena</span></td>
                              <td><span ng-bind="g.preduzece.naziv"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.godina.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="godina">Godina</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.godina.godina" id="godina" class="oznaka form-control input-sm" placeholder="Unesite oznaku drzave" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 4}}" ng-maxlength="4"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.godina.$error.required">This is a required field</span>
                                      <span ng-show="myForm.godina.$error.minlength">Minimum length required is 4</span>
                                      <span ng-show="myForm.godina.$error.maxlength">Maximum length required is 4</span>
                                      <span ng-show="myForm.godina.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                                                
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="zakljucena">Zakljucena</label>
                              <div class="col-md-7">
                              	<md-checkbox ng-model="ctrl.godina.zakljucena" ng-init="false" aria-label="Zakljucena"></md-checkbox>
                              </div>
                          </div>
                      </div>
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="preduzece">Preduzece</label>
                              <div class="col-md-7">
                              	<label class="col-md-2 control-lable" id="preduzece">{{ctrl.godina.preduzece.naziv}}</label>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit" id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                              <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
 
      </div>
      </div>
