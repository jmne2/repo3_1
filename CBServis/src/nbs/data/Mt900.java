package nbs.data;

import java.sql.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import annotations.SqlDateAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mt900", 
		namespace="http://nbs/data", 
		propOrder = {"idPoruke",
					 "swiftBankeD",
					 "racunBankeD",
					 "idPorukeNaloga",
					 "datumValute",
					 "iznos", 
					 "sifraValute"})
public class Mt900 implements java.io.Serializable{

	@XmlElement(name="idPoruke", required = true)
	@Size(min = 1, max = 50)
	private String idPoruke;
	
	//swift kod banke duznika
	@XmlElement(name="swiftBankeD", required = true)
	@Size(min = 8, max = 8)
	private String swiftBankeD;
	
	//obracunski racun banke duznika
	@XmlElement(name="racunBankeD", required = true)
	@Size(min = 18, max = 18)
	private String racunBankeD;
	
	//id poruke naloga (MT103 ili MT102)
	@XmlElement(name="idPorukeNaloga", required = true)
	@Size(min = 1, max = 50)
	private String idPorukeNaloga;
	
	@XmlElement(name="datumValute", required = true)
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date datumValute;
	
	@XmlElement(name="iznos", required = true)
	@Digits(integer = 15, fraction = 2)
	private double iznos;

	@XmlElement(name="sifraValute", required = true)
	@Size(min = 3, max = 3)
	private String sifraValute;

	public Mt900(){
		
	}

	public Mt900(String idPoruke, String swiftBankeD, String racunBankeD, String idPorukeNaloga, Date datumValute,
			double iznos, String sifraValute) {
		super();
		this.idPoruke = idPoruke;
		this.swiftBankeD = swiftBankeD;
		this.racunBankeD = racunBankeD;
		this.idPorukeNaloga = idPorukeNaloga;
		this.datumValute = datumValute;
		this.iznos = iznos;
		this.sifraValute = sifraValute;
	}

	public String getIdPoruke() {
		return idPoruke;
	}

	public void setIdPoruke(String idPoruke) {
		this.idPoruke = idPoruke;
	}

	public String getSwiftBankeD() {
		return swiftBankeD;
	}

	public void setSwiftBankeD(String swiftBankeD) {
		this.swiftBankeD = swiftBankeD;
	}

	public String getRacunBankeD() {
		return racunBankeD;
	}

	public void setRacunBankeD(String racunBankeD) {
		this.racunBankeD = racunBankeD;
	}

	public String getIdPorukeNaloga() {
		return idPorukeNaloga;
	}

	public void setIdPorukeNaloga(String idPorukeNaloga) {
		this.idPorukeNaloga = idPorukeNaloga;
	}

	public Date getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public String getSifraValute() {
		return sifraValute;
	}

	public void setSifraValute(String sifraValute) {
		this.sifraValute = sifraValute;
	}
	
	
}
