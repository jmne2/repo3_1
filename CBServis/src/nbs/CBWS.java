package nbs;

import java.util.ArrayList;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;

import nbs.data.Mt103;
import nbs.data.Mt900;
import nbs.data.Nalog;

@WebService(name = "CBWS",
targetNamespace = "http://nbs")
public interface CBWS {

	@WebMethod(operationName = "getNalog")  	
	public Mt900 getNalog(@WebParam(name = "mt103") Mt103 mt103);	//prima nalog, prenosi sredstva sa racuna banke 1 na racun druge banke
	//vraca poruku o zaduzenju mt900
	// kad banka primi poruku o zaduzenju skida pare s racuna klijenta
	
	@WebMethod(operationName = "sendNalog")  	
	public void sendNalog(@WebParam(name = "mt103") Mt103 mt103);	
	//prosledjuje nalog drugoj banci i salje poruku o odobrenju mt910
	// kad banka primi poruku o odobrenju dodaju se pare na racun njenog klijenta
	
	@WebMethod(operationName = "settle")  	
	public Mt900 settle(@WebParam(name = "nalozi") ArrayList<Nalog> nalozi, @WebParam(name = "datumValute") XMLGregorianCalendar datumValute) throws Exception;
	// prima listu naloga iz jedne banke (namenjenih za uplatu drugoj banci)
	// trazi od druge banke listu namenjenu za prvu banku
	// sabira i oduzima sve
	// salje prvoj banci poruku o zaduzenju MT900
	// salje drugoj banci MT102 i MT910
	
	// banke razduzuju klijente
	
	
}
