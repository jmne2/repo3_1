
package izvod;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import izvod.data.NalogBanka;


/**
 * <p>Java class for sendNalog complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendNalog">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nalogZaPlacanje" type="{http://izvod/data}nalogBanka" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendNalog324", propOrder = {
    "nalogZaPlacanje"
})
public class SendNalog {

    protected NalogBanka nalogZaPlacanje;

    /**
     * Gets the value of the nalogZaPlacanje property.
     * 
     * @return
     *     possible object is
     *     {@link NalogBanka }
     *     
     */
    public NalogBanka getNalogZaPlacanje() {
        return nalogZaPlacanje;
    }

    /**
     * Sets the value of the nalogZaPlacanje property.
     * 
     * @param value
     *     allowed object is
     *     {@link NalogBanka }
     *     
     */
    public void setNalogZaPlacanje(NalogBanka value) {
        this.nalogZaPlacanje = value;
    }

}
