package izvod;

import java.util.ArrayList;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import izvod.data.Izvod;
import izvod.data.Mt102;
import izvod.data.Mt103;
import izvod.data.Mt910;
import izvod.data.Nalog;
import izvod.data.NalogBanka;
import izvod.data.ZahtevZaIzvod;
import nbs.data.Mt900;


@WebService(name = "IzvodWS",
			targetNamespace = "http://izvod")
public interface IzvodWS {

	@WebMethod(operationName = "getIzvod")  	
	public Izvod getIzvod(@WebParam(name = "zahtevZaIzvod") ZahtevZaIzvod zahtevZaIzvod);	

	@WebMethod(operationName = "sendNalog")
	public void sendNalog(@WebParam(name="nalogZaPlacanje") NalogBanka nalogZaPlacanje);
	
	@WebMethod(operationName= "setBankName")
	public void setBankName(@WebParam(name="bankName") String bankName);
	
	@WebMethod(operationName= "setBankCode")
	public void setBankCode(@WebParam(name="bankCode") String bankCode);
	
	@WebMethod(operationName= "receiveNalogNbs")
	public void receiveNalogNbs(@WebParam(name="mt103") Mt103 mt103, @WebParam(name="mt910") Mt910 mt910);
	
	@WebMethod(operationName= "clearing")
	public void clearing(@WebParam(name="bankCode2") String bankCode2);

	@WebMethod(operationName= "clearing2")
	public ArrayList<Nalog> clearing2(@WebParam(name="bankCode") String bankCode);

	@WebMethod(operationName= "receiveMT102AndMT910")
	public void receiveMT102AndMT910(@WebParam(name="mt102") Mt102 mt102, @WebParam(name="mt910") Mt910 mt910 );

}
