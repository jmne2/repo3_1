package izvod.data;

import java.sql.Date;
import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import annotations.SqlDateAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "izvod", 
		namespace="http://izvod/data", 
		propOrder = {"brRacuna",
					 "datumNaloga",
					 "brPreseka",
					 "prethodnoStanje",
					 "brPromenaUKorist",
					 "ukupnoUKorist",
					 "brPromenaNaTeret",
					 "ukupnoNaTeret",
					 "novoStanje",
					 "stavke"})

public class Izvod  implements java.io.Serializable {
//	private static final long serialVersionUID = 4575678568872321L;
	
	@XmlTransient
	private int id;
	
	@XmlElement(name="brRacuna", required = true)
	@Size(min = 18, max = 18)
	private String brRacuna;
	
	@XmlElement(name="datumNaloga", required = true)
	@XmlJavaTypeAdapter(SqlDateAdapter.class)
    private Date datumNaloga;
	
	@XmlElement(name="brPreseka", required = true)	
	@Digits(integer = 2, fraction = 0)	
    private int brPreseka;
	
	@XmlElement(name="prethodnoStanje", required = false)
	@Digits(integer = 15, fraction = 2)	
	private double prethodnoStanje;
	
	@XmlElement(name="brPromenaUKorist", required = false)
	@Digits(integer = 6, fraction = 0)	
	private int brPromenaUKorist;
	
	@XmlElement(name="ukupnoUKorist", required = false)
	@Digits(integer = 15, fraction = 2)	
	private double ukupnoUKorist;
	
	@XmlElement(name="brPromenaNaTeret", required = false)
	@Digits(integer = 6, fraction = 0)	
	private int brPromenaNaTeret;
	
	@XmlElement(name="ukupnoNaTeret", required = false)
	@Digits(integer = 15, fraction = 2)	
	private double ukupnoNaTeret;
	
	@XmlElement(name="novoStanje", required = false)
	@Digits(integer = 15, fraction = 2)	
	private double novoStanje;
	
	@XmlElementWrapper(name="stavke", required=false)
	@XmlElement(name="stavke", required = false)
	private List<StavkaIzvodaBanka> stavke;
	
	public Izvod(){}

	public Izvod(String brRacuna, Date datumNaloga, int brPreseka, double prethodnoStanje, int brPromenaUKorist,
			double ukupnoUKorist, int brPromenaNaTeret, double ukupnoNaTeret, double novoStanje,
			List<StavkaIzvodaBanka> stavke) {
		super();
		this.brRacuna = brRacuna;
		this.datumNaloga = datumNaloga;
		this.brPreseka = brPreseka;
		this.prethodnoStanje = prethodnoStanje;
		this.brPromenaUKorist = brPromenaUKorist;
		this.ukupnoUKorist = ukupnoUKorist;
		this.brPromenaNaTeret = brPromenaNaTeret;
		this.ukupnoNaTeret = ukupnoNaTeret;
		this.novoStanje = novoStanje;
		this.stavke = stavke;
	}

	public String getBrRacuna() {
		return brRacuna;
	}

	public void setBrRacuna(String brRacuna) {
		this.brRacuna = brRacuna;
	}

	public Date getDatumNaloga() {
		return datumNaloga;
	}

	public void setDatumNaloga(Date datumNaloga) {
		this.datumNaloga = datumNaloga;
	}

	public int getBrPreseka() {
		return brPreseka;
	}

	public void setBrPreseka(int brPreseka) {
		this.brPreseka = brPreseka;
	}

	public double getPrethodnoStanje() {
		return prethodnoStanje;
	}

	public void setPrethodnoStanje(double prethodnoStanje) {
		this.prethodnoStanje = prethodnoStanje;
	}

	public int getBrPromenaUKorist() {
		return brPromenaUKorist;
	}

	public void setBrPromenaUKorist(int brPromenaUKorist) {
		this.brPromenaUKorist = brPromenaUKorist;
	}

	public double getUkupnoUKorist() {
		return ukupnoUKorist;
	}

	public void setUkupnoUKorist(double ukupnoUKorist) {
		this.ukupnoUKorist = ukupnoUKorist;
	}

	public int getBrPromenaNaTeret() {
		return brPromenaNaTeret;
	}

	public void setBrPromenaNaTeret(int brPromenaNaTeret) {
		this.brPromenaNaTeret = brPromenaNaTeret;
	}

	public double getUkupnoNaTeret() {
		return ukupnoNaTeret;
	}

	public void setUkupnoNaTeret(double ukupnoNaTeret) {
		this.ukupnoNaTeret = ukupnoNaTeret;
	}

	public double getNovoStanje() {
		return novoStanje;
	}

	public void setNovoStanje(double novoStanje) {
		this.novoStanje = novoStanje;
	}

	public List<StavkaIzvodaBanka> getStavke() {
		return stavke;
	}

	public void setStavke(List<StavkaIzvodaBanka> stavke) {
		this.stavke = stavke;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
