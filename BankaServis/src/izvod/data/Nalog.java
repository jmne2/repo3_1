
package izvod.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nalog", propOrder = {
    "idNaloga",
    "uplatilac",
    "svrhaUplate",
    "primalac",
    "datumNaloga",
    "racunUplatioca",
    "modelZaduzenja",
    "pozivNaBrojZaduzenja",
    "racunPrimaoca",
    "modelOdobrenja",
    "pozivNaBrojOdobrenja",
    "iznos",
    "oznakaValute"
})
public class Nalog {

    @XmlElement(required = true)
    private String idNaloga;
    @XmlElement(required = true)
    private String uplatilac;
    @XmlElement(required = true)
    private String svrhaUplate;
    @XmlElement(required = true)
    private String primalac;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    private XMLGregorianCalendar datumNaloga;
    @XmlElement(required = true)
    private String racunUplatioca;
    private int modelZaduzenja;
    @XmlElement(required = true)
    private String pozivNaBrojZaduzenja;
    @XmlElement(required = true)
    private String racunPrimaoca;
    private int modelOdobrenja;
    @XmlElement(required = true)
    private String pozivNaBrojOdobrenja;
    private double iznos;
    @XmlElement(required = true)
    private String oznakaValute;
	public Nalog(String idNaloga, String uplatilac, String svrhaUplate, String primalac,
			XMLGregorianCalendar datumNaloga, String racunUplatioca, int modelZaduzenja, String pozivNaBrojZaduzenja,
			String racunPrimaoca, int modelOdobrenja, String pozivNaBrojOdobrenja, double iznos, String oznakaValute) {
		super();
		this.idNaloga = idNaloga;
		this.uplatilac = uplatilac;
		this.svrhaUplate = svrhaUplate;
		this.primalac = primalac;
		this.datumNaloga = datumNaloga;
		this.racunUplatioca = racunUplatioca;
		this.modelZaduzenja = modelZaduzenja;
		this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
		this.racunPrimaoca = racunPrimaoca;
		this.modelOdobrenja = modelOdobrenja;
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
		this.iznos = iznos;
		this.oznakaValute = oznakaValute;
	}

   public Nalog(){}

public String getIdNaloga() {
	return idNaloga;
}

public void setIdNaloga(String idNaloga) {
	this.idNaloga = idNaloga;
}

public String getUplatilac() {
	return uplatilac;
}

public void setUplatilac(String uplatilac) {
	this.uplatilac = uplatilac;
}

public String getSvrhaUplate() {
	return svrhaUplate;
}

public void setSvrhaUplate(String svrhaUplate) {
	this.svrhaUplate = svrhaUplate;
}

public String getPrimalac() {
	return primalac;
}

public void setPrimalac(String primalac) {
	this.primalac = primalac;
}

public XMLGregorianCalendar getDatumNaloga() {
	return datumNaloga;
}

public void setDatumNaloga(XMLGregorianCalendar datumNaloga) {
	this.datumNaloga = datumNaloga;
}

public String getRacunUplatioca() {
	return racunUplatioca;
}

public void setRacunUplatioca(String racunUplatioca) {
	this.racunUplatioca = racunUplatioca;
}

public int getModelZaduzenja() {
	return modelZaduzenja;
}

public void setModelZaduzenja(int modelZaduzenja) {
	this.modelZaduzenja = modelZaduzenja;
}

public String getPozivNaBrojZaduzenja() {
	return pozivNaBrojZaduzenja;
}

public void setPozivNaBrojZaduzenja(String pozivNaBrojZaduzenja) {
	this.pozivNaBrojZaduzenja = pozivNaBrojZaduzenja;
}

public String getRacunPrimaoca() {
	return racunPrimaoca;
}

public void setRacunPrimaoca(String racunPrimaoca) {
	this.racunPrimaoca = racunPrimaoca;
}

public int getModelOdobrenja() {
	return modelOdobrenja;
}

public void setModelOdobrenja(int modelOdobrenja) {
	this.modelOdobrenja = modelOdobrenja;
}

public String getPozivNaBrojOdobrenja() {
	return pozivNaBrojOdobrenja;
}

public void setPozivNaBrojOdobrenja(String pozivNaBrojOdobrenja) {
	this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
}

public double getIznos() {
	return iznos;
}

public void setIznos(double iznos) {
	this.iznos = iznos;
}

public String getOznakaValute() {
	return oznakaValute;
}

public void setOznakaValute(String oznakaValute) {
	this.oznakaValute = oznakaValute;
}
   
   

}
