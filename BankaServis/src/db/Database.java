package db;

import java.io.EOFException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import izvod.data.Izvod;
import izvod.data.Mt102;
import izvod.data.Mt910;
import izvod.data.NalogBanka;
import izvod.data.StavkaIzvodaBanka;
import izvod.data.ZahtevZaIzvod;
import nbs.CBWS;
import nbs.data.Mt103;
import nbs.data.Mt900;
import nbs.data.Nalog;


public class Database {
	private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private String bankCode = "";

    public Database(String bankCode){
    	this.bankCode = bankCode;
    	try {
    		
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbcbank" + bankCode + "?" + "user=root&password=admin123");
			statement = connect.createStatement();
	  
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
    
    public Izvod readDataBase(ZahtevZaIzvod zahtev) throws Exception {
        try {
        	Date datum = zahtev.getDatum();	
        	String brRacuna = zahtev.getBrojRacuna();
     //       String dan = String.valueOf(datum.getDate())+"-"+String.valueOf(datum.getMonth()+1)+"-"+String.valueOf(datum.getYear()+1900);
       
            String query = "select * from IZVOD where BRRACUNA = ? and DATUMNALOGA = ? ";
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setString(1, brRacuna);
            preparedStatement.setString(2, datum.toString());
            resultSet = preparedStatement.executeQuery();

            Izvod izvod = null;
            if (resultSet.next()){
            	izvod = new Izvod();
	            izvod.setBrPreseka(resultSet.getInt("brPreseka"));
	            izvod.setBrRacuna(brRacuna);
	            izvod.setDatumNaloga(resultSet.getDate("datumNaloga"));
	            izvod.setPrethodnoStanje(Double.valueOf(resultSet.getString("prethodnoStanje")));
	            izvod.setBrPromenaUKorist(Integer.valueOf(resultSet.getString("brPromenaUKorist")));
	            izvod.setUkupnoUKorist(Double.valueOf(resultSet.getString("ukupnoUKorist")));
	            izvod.setBrPromenaNaTeret(Integer.valueOf(resultSet.getString("brPromenaNaTeret")));
	            izvod.setUkupnoNaTeret(Double.valueOf(resultSet.getString("ukupnoNaTeret")));
	            izvod.setNovoStanje(Double.valueOf(resultSet.getString("novoStanje")));
	            izvod.setId(resultSet.getInt("idizvod"));
	            List<StavkaIzvodaBanka> stavke = new ArrayList<StavkaIzvodaBanka>();
	            int id = resultSet.getInt("idizvod");
	            izvod.setId(id);
	            String query2 = "select * from STAVKAIZVODABANKA where IDSTAVKAIZVODABANKA= ? ";
	            preparedStatement = connect.prepareStatement(query2);
	            preparedStatement.setString(1, String.valueOf(id));
	            ResultSet resultSet2 = preparedStatement.executeQuery();
            
	            while (resultSet2.next()) {
	            	StavkaIzvodaBanka sib = new StavkaIzvodaBanka();
	            	sib.setUplatilac(resultSet2.getString("uplatilac"));
	            	sib.setSvrhaPlacanja(resultSet2.getString("svrhaPlacanja"));
	            	sib.setPrimalac(resultSet2.getString("primalac"));
	            	sib.setDatumNaloga(resultSet2.getDate("datumNaloga"));
	            	System.out.println("Datum je " + sib.getDatumNaloga());
	            	sib.setDatumValute(resultSet2.getDate("datumValute"));
	            	sib.setRacunUplatioca(resultSet2.getString("racunUplatioca"));
	            	sib.setModelZaduzenja(resultSet2.getInt("modelZaduzenja"));
	            	sib.setPozivNaBrojZaduzenja(resultSet2.getString("pozivNaBrojZaduzenja"));
	            	sib.setRacunPrimaoca(resultSet2.getString("racunPrimaoca"));
	            	sib.setModelOdobrenja(resultSet2.getInt("modelOdobrenja"));
	            	sib.setPozivNaBrojOdobrenja(resultSet2.getString("pozivNaBrojOdobrenja"));
	            	sib.setIznos(resultSet2.getDouble("iznos"));
	            	sib.setSmer(resultSet2.getString("smer"));
	            	stavke.add(sib);
	            }
	            izvod.setStavke(stavke);
            }
            return izvod;
        } catch (Exception e) {
            throw e;
        } finally {
          //  close();
        }

    }
   
    // banci stize nalog, ako je racun primaoca u toj banci izvrsice se odmah prenos sredstava
    // ako nije u toj banci, rezervisace se novac kod uplatioca, poslace se mt103 centralnoj banci (getNalog()) koja ce vratiti
    // mt900 i skinuce se novac uplatiocu
    // Ako je clearing u pitanju: sacuvace se nalog u bazi, cekace se poziv metode clear()
    public void readNalogBanka(NalogBanka nalog) throws Exception{
    	System.out.println("**********ENTERED READ NALOG BANKA **************");
    	String racunPrimaoca = nalog.getRacunPrimaoca();
    	String racunUplatioca = nalog.getRacunUplatioca();
    	if(proveriRacun(racunPrimaoca)){
    		String smer = "P";
    		Izvod izvod = createStavka(nalog, smer);
            
            String query = "UPDATE IZVOD SET prethodnostanje = ? , brpromenaukorist = ? , ukupnoukorist = ? , novostanje = ? WHERE idizvod = ? ";
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setDouble(1, izvod.getNovoStanje());
            preparedStatement.setInt(2, izvod.getBrPromenaUKorist()+1);
            preparedStatement.setDouble(3, izvod.getUkupnoUKorist()+ nalog.getIznos());
            preparedStatement.setDouble(4, izvod.getNovoStanje()+ nalog.getIznos());
            preparedStatement.setString(5, String.valueOf(izvod.getId()));
            preparedStatement.executeUpdate();
            
           smer = "D";
     	   izvod = createStavka(nalog, smer);
            String query2 = "UPDATE IZVOD SET prethodnostanje = ? , brpromenanateret = ? , ukupnonateret = ? , novostanje = ? WHERE idizvod = ? ";
            preparedStatement = connect.prepareStatement(query2);
            preparedStatement.setDouble(1, izvod.getNovoStanje());
            preparedStatement.setInt(2, izvod.getBrPromenaNaTeret()+1);
            preparedStatement.setDouble(3, izvod.getUkupnoNaTeret()+ nalog.getIznos());
            preparedStatement.setDouble(4, izvod.getNovoStanje()-nalog.getIznos());
            preparedStatement.setInt(5, izvod.getId());
            preparedStatement.executeUpdate();
    	}
    	else{
    		if(nalog.isHitno() || (nalog.getIznos()>=250000 && nalog.getOznakaValute().equals("RSD"))){
    			
    			rezervisiNovac(racunUplatioca, nalog.getIznos());
    			
    			URL wsdlLocation = new URL("http://localhost:8082/nbs-ws/services/CBWS?wsdl");
				QName serviceName = new QName("http://nbs", "CBWSService");
				QName portName = new QName("http://nbs", "CBWSPort");

				Service service = Service.create(wsdlLocation, serviceName);
				
				CBWS cbws = service.getPort(portName, CBWS.class);
				GregorianCalendar c = new GregorianCalendar();
				c.setTime(nalog.getDatumNaloga());
				XMLGregorianCalendar date1 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				c.setTime(nalog.getDatumValute());
				XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				
				Mt103 mt103 = new Mt103("idPoruke567", "12345678", "123456789123456789", "23456789", "123456789123456788", nalog.getUplatilac(), nalog.getSvrhaUplate(), nalog.getPrimalac(), date1, date2, nalog.getRacunUplatioca(), nalog.getModelZaduzenja(), nalog.getPozivNaBrojZaduzenja(), nalog.getRacunPrimaoca(), nalog.getModelOdobrenja(), nalog.getPozivNaBrojOdobrenja(), nalog.getIznos(), nalog.getOznakaValute());
				Mt900 mt900 = cbws.getNalog(mt103);
				System.out.println("*****I GOT A RESPONSE : " + mt900.getIznos() +" dinara ***************");
    	
				skiniNovac(mt103);
    		}
    		else{
    			saveNalogForClearing(nalog);
    		}
    	}
    	close();
    }
    
    private Izvod createStavka(NalogBanka nalog, String smer){
    	ZahtevZaIzvod zahtev = null;
    	String brRacuna = "";
    	if(smer.equals("D")){
    		brRacuna = nalog.getRacunUplatioca();
    		zahtev = new ZahtevZaIzvod(brRacuna, nalog.getDatumNaloga(), 0);
    	}
    	else if(smer.equals("P")){
    		brRacuna = nalog.getRacunPrimaoca();
    		zahtev = new ZahtevZaIzvod(brRacuna, nalog.getDatumNaloga(), 0);
    	}
    	Izvod izvod = null;
		try {
			izvod = readDataBase(zahtev);
			if(izvod==null){
				String query = "INSERT INTO IZVOD VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			       preparedStatement = connect.prepareStatement(query);
			       preparedStatement.setString(1, null);
			       preparedStatement.setString(2, brRacuna);
			       preparedStatement.setDate(3, nalog.getDatumNaloga());
			       preparedStatement.setInt(4, 0);
			       preparedStatement.setDouble(5, 0);
			       preparedStatement.setInt(6, 0);
			       preparedStatement.setDouble(7, 0);
			       preparedStatement.setInt(8, 0);
			       preparedStatement.setDouble(9, 0);
			       preparedStatement.setDouble(10, 0);
			       preparedStatement.executeUpdate();
			       izvod = readDataBase(zahtev);
			}
			
			StavkaIzvodaBanka sib = new StavkaIzvodaBanka();
	        sib.setId(izvod.getId());
	        sib.setUplatilac(nalog.getUplatilac());
	        sib.setSvrhaPlacanja(nalog.getSvrhaUplate());
	        sib.setPrimalac(nalog.getPrimalac());
	        sib.setDatumNaloga(nalog.getDatumNaloga());
	        sib.setDatumValute(nalog.getDatumValute());
	        sib.setRacunUplatioca(nalog.getRacunUplatioca());
	        sib.setModelZaduzenja(nalog.getModelZaduzenja());
	        sib.setPozivNaBrojZaduzenja(nalog.getPozivNaBrojZaduzenja());
	        sib.setRacunPrimaoca(nalog.getRacunPrimaoca());
	        sib.setModelOdobrenja(nalog.getModelOdobrenja());
	        sib.setPozivNaBrojOdobrenja(nalog.getPozivNaBrojOdobrenja());
	        sib.setIznos(nalog.getIznos());
	        sib.setSmer(smer);
	       String query = "INSERT INTO STAVKAIZVODABANKA VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	       preparedStatement = connect.prepareStatement(query);
	       preparedStatement.setString(1, null);
	       preparedStatement.setString(2, sib.getUplatilac());
	       preparedStatement.setString(3, sib.getSvrhaPlacanja());
	       preparedStatement.setString(4, sib.getPrimalac());
	       preparedStatement.setDate(5, sib.getDatumNaloga());
	       preparedStatement.setDate(6, sib.getDatumValute());
	       preparedStatement.setString(7, sib.getRacunUplatioca());
	       preparedStatement.setInt(8, sib.getModelZaduzenja());
	       preparedStatement.setString(9, sib.getPozivNaBrojZaduzenja());
	       preparedStatement.setString(10, sib.getRacunPrimaoca());
	       preparedStatement.setInt(11, sib.getModelOdobrenja());
	       preparedStatement.setString(12, sib.getPozivNaBrojOdobrenja());
	       preparedStatement.setDouble(13, sib.getIznos());
	       preparedStatement.setString(14, sib.getSmer());
	       preparedStatement.setInt(15, sib.getId());
	       
	       preparedStatement.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return izvod;
    }
    
    private boolean proveriRacun(String racun){
    	String banka = racun.substring(0, 3);
    	String code = bankCode;
    	if(banka.equals(code)){
    		  String query = "select * from RACUNI_KLIJENATA where BR_RACUNA = ?";
              try {
				preparedStatement = connect.prepareStatement(query);
				preparedStatement.setString(1, racun);
	            resultSet = preparedStatement.executeQuery();

	            if (resultSet.next()){
	  	           return true;
	            }
	            else return false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	return false;
    }
  
    
    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

    // uplacuje novac primaocu na osnovu mt103 i mt910
	public void finishTransaction(izvod.data.Mt103 mt103, Mt910 mt910) throws SQLException {
		System.out.println("***********FINISHING TRANSACTION -> PUTTING " + mt103.getIznos() + " TO " + mt103.getRacunPrimaoca()+"**********");
		String brRacuna = mt103.getRacunPrimaoca();
		Double iznos = mt103.getIznos(); 
		java.util.Date date = new java.util.Date();
		Date datum = new Date(date.getTime());
		
		String query = "select * from IZVOD where BRRACUNA = ? and DATUMNALOGA = ? ";
        preparedStatement = connect.prepareStatement(query);
        preparedStatement.setString(1, brRacuna);
        preparedStatement.setString(2, datum.toString());
        resultSet = preparedStatement.executeQuery();

        int idIzvoda = 0;
        double prethodnoStanje = 0;
        int brPromUKor = 0;
        double ukupnoUKorist = 0;
        if (resultSet.next()){
        	idIzvoda = resultSet.getInt("idizvod");
        	prethodnoStanje = resultSet.getDouble("novostanje");
        	brPromUKor = resultSet.getInt("brPromenaUKorist");
        	ukupnoUKorist = resultSet.getDouble("ukupnoUKorist");
        }
	/*	java.util.Date d = mt103.getDatumNaloga().toGregorianCalendar().getTime();
        Date date1 = new Date(d.getTime());
        d = mt103.getDatumValute().toGregorianCalendar().getTime();
        Date date2 = new Date(d.getTime());
      */  

        if(idIzvoda==0){
				String query4 = "INSERT INTO IZVOD VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			       preparedStatement = connect.prepareStatement(query4);
			       preparedStatement.setString(1, null);
			       preparedStatement.setString(2, brRacuna);
			       preparedStatement.setDate(3, mt103.getDatumNaloga());
			       preparedStatement.setInt(4, 0);
			       preparedStatement.setDouble(5, 0);
			       preparedStatement.setInt(6, 0);
			       preparedStatement.setDouble(7, 0);
			       preparedStatement.setInt(8, 0);
			       preparedStatement.setDouble(9, 0);
			       preparedStatement.setDouble(10, 0);
			       preparedStatement.executeUpdate();
			       
			       String query5 = "select * from IZVOD where BRRACUNA = ? and DATUMNALOGA = ? ";
			        preparedStatement = connect.prepareStatement(query5);
			        preparedStatement.setString(1, brRacuna);
			        preparedStatement.setString(2, datum.toString());
			        resultSet = preparedStatement.executeQuery();

			        if (resultSet.next()){
			        	idIzvoda = resultSet.getInt("idizvod");
			        	prethodnoStanje = resultSet.getDouble("novostanje");
			        	brPromUKor = resultSet.getInt("brPromenaUKorist");
			        	ukupnoUKorist = resultSet.getDouble("ukupnoUKorist"); 
			        }
        }
        
		String query2 = "INSERT INTO STAVKAIZVODABANKA VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	       preparedStatement = connect.prepareStatement(query2);
	       preparedStatement.setString(1, null);
	       preparedStatement.setString(2, mt103.getDuznik());
	       preparedStatement.setString(3, mt103.getSvrhaPlacanja());
	       preparedStatement.setString(4, mt103.getPrimalac());
	       preparedStatement.setDate(5, mt103.getDatumNaloga());
	       preparedStatement.setDate(6, mt103.getDatumValute());
	       preparedStatement.setString(7, mt103.getRacunDuznika());
	       preparedStatement.setInt(8, mt103.getModelZaduzenja());
	       preparedStatement.setString(9, mt103.getPozivNaBrojZaduzenja());
	       preparedStatement.setString(10, mt103.getRacunPrimaoca());
	       preparedStatement.setInt(11, mt103.getModelOdobrenja());
	       preparedStatement.setString(12, mt103.getPozivNaBrojOdobrenja());
	       preparedStatement.setDouble(13, iznos);
	       preparedStatement.setString(14, "P");
	       preparedStatement.setInt(15, idIzvoda);
	       
	       preparedStatement.executeUpdate();
       String query3 = "UPDATE IZVOD SET prethodnostanje = ? , brpromenaukorist = ? , ukupnoukorist = ? , novostanje = ? WHERE idizvod = ? ";
       preparedStatement = connect.prepareStatement(query3);
       preparedStatement.setDouble(1, prethodnoStanje);
       preparedStatement.setInt(2, brPromUKor+1);
       preparedStatement.setDouble(3, ukupnoUKorist + iznos);
       preparedStatement.setDouble(4, prethodnoStanje +iznos);
       preparedStatement.setInt(5, idIzvoda);
       preparedStatement.executeUpdate();
	}

	public void rezervisiNovac(String brRacuna, double iznos) throws SQLException{
		String query = "UPDATE RACUNI_KLIJENATA SET rezervisano = ? WHERE br_racuna = ? ";
       preparedStatement = connect.prepareStatement(query);
       preparedStatement.setDouble(1, iznos);		//ne gleda staro rezervisano stanje
       preparedStatement.setString(2, brRacuna);
       preparedStatement.executeUpdate();
	}
	
	public void skiniNovac(nbs.data.Mt103 mt103) throws SQLException{
		String brRacuna = mt103.getRacunDuznika();
		Double iznos = mt103.getIznos(); 
		java.util.Date date = new java.util.Date();
		Date datum = new Date(date.getTime());
		
		String query = "select * from IZVOD where BRRACUNA = ? and DATUMNALOGA = ? ";
        preparedStatement = connect.prepareStatement(query);
        preparedStatement.setString(1, brRacuna);
        preparedStatement.setString(2, datum.toString());
        resultSet = preparedStatement.executeQuery();

        java.util.Date d = mt103.getDatumNaloga().toGregorianCalendar().getTime();
        Date date1 = new Date(d.getTime());
        d = mt103.getDatumValute().toGregorianCalendar().getTime();
        Date date2 = new Date(d.getTime());
        
        int idIzvoda = 0;
        double prethodnoStanje = 0;
        int brPromNaTeret = 0;
        double ukupnoNaTeret = 0;
        if (resultSet.next()){
        	idIzvoda = resultSet.getInt("idizvod");
        	prethodnoStanje = resultSet.getDouble("novostanje");
        	brPromNaTeret = resultSet.getInt("brPromenaNaTeret");
        	ukupnoNaTeret = resultSet.getDouble("ukupnoNaTeret");
        }
		
        
		String query2 = "INSERT INTO STAVKAIZVODABANKA VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	       preparedStatement = connect.prepareStatement(query2);
	       preparedStatement.setString(1, null);
	       preparedStatement.setString(2, mt103.getDuznik());
	       preparedStatement.setString(3, mt103.getSvrhaPlacanja());
	       preparedStatement.setString(4, mt103.getPrimalac());
	       preparedStatement.setDate(5, date1);
	       preparedStatement.setDate(6, date2);
	       preparedStatement.setString(7, mt103.getRacunDuznika());
	       preparedStatement.setInt(8, mt103.getModelZaduzenja());
	       preparedStatement.setString(9, mt103.getPozivNaBrojZaduzenja());
	       preparedStatement.setString(10, mt103.getRacunPrimaoca());
	       preparedStatement.setInt(11, mt103.getModelOdobrenja());
	       preparedStatement.setString(12, mt103.getPozivNaBrojOdobrenja());
	       preparedStatement.setDouble(13, iznos);
	       preparedStatement.setString(14, "D");
	       preparedStatement.setInt(15, idIzvoda);
	       
	       preparedStatement.executeUpdate();
       String query3 = "UPDATE IZVOD SET prethodnostanje = ? , brpromenaukorist = ? , ukupnoukorist = ? , novostanje = ? WHERE idizvod = ? ";
       preparedStatement = connect.prepareStatement(query3);
       preparedStatement.setDouble(1, prethodnoStanje);
       preparedStatement.setInt(2, brPromNaTeret+1);
       preparedStatement.setDouble(3, ukupnoNaTeret + iznos);
       preparedStatement.setDouble(4, prethodnoStanje -iznos);
       preparedStatement.setInt(5, idIzvoda);
       preparedStatement.executeUpdate();
	}
	
	  
    private void saveNalogForClearing(NalogBanka nalog) throws SQLException{
    	String query = "INSERT INTO NALOGBANKA VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	       preparedStatement = connect.prepareStatement(query);
	       preparedStatement.setString(1, nalog.getIdPoruke());
	       preparedStatement.setString(2, nalog.getUplatilac());
	       preparedStatement.setString(3, nalog.getSvrhaUplate());
	       preparedStatement.setString(4, nalog.getPrimalac());
	       preparedStatement.setDate(5, nalog.getDatumNaloga());
	       preparedStatement.setDate(6, nalog.getDatumValute());
	       preparedStatement.setString(7, nalog.getRacunUplatioca());
	       preparedStatement.setInt(8, nalog.getModelZaduzenja());
	       preparedStatement.setString(9, nalog.getPozivNaBrojZaduzenja());
	       preparedStatement.setString(10, nalog.getRacunPrimaoca());
	       preparedStatement.setInt(11, nalog.getModelOdobrenja());
	       preparedStatement.setString(12, nalog.getPozivNaBrojOdobrenja());
	       preparedStatement.setDouble(13, nalog.getIznos());
	       preparedStatement.setString(14, nalog.getOznakaValute());
	       preparedStatement.setInt(15, nalog.isHitno()? 1:0);
	       preparedStatement.setString(16, null);
	       preparedStatement.setString(17, nalog.getRacunPrimaoca().substring(0, 3));
	       preparedStatement.executeUpdate();
    }
	
    
    // pozvace se metoda settle u centralnoj banci i poslace joj se svi nalozi za clearing
	public void clear(String bankCode2) throws SQLException, MalformedURLException, DatatypeConfigurationException{
		String query = "select * from nalogbanka where bankCodePrimaoca = ? ";
        preparedStatement = connect.prepareStatement(query);
        preparedStatement.setString(1, bankCode2);
        resultSet = preparedStatement.executeQuery();
        
        ArrayList<NalogBanka> naloziBanke = new ArrayList<NalogBanka>();
        ArrayList<Nalog> nalozi = new ArrayList<Nalog>();
        
       System.out.println("**********COLLECTING TRANSFER ORDERS FOR BANK " + bankCode2 + "*************");
        
        while(resultSet.next()){
        	GregorianCalendar c = new GregorianCalendar();
			c.setTime(resultSet.getDate("datumNaloga"));
			XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			
        	Nalog n = new Nalog(resultSet.getString("idNalogBanka"), resultSet.getString("uplatilac"), resultSet.getString("svrhaUplate"), resultSet.getString("primalac"), date, resultSet.getString("racunUplatioca"), resultSet.getInt("modelZaduzenja"), resultSet.getString("pozivNaBrojZaduzenja"), resultSet.getString("racunPrimaoca"), resultSet.getInt("modelOdobrenja"), resultSet.getString("pozivNaBrojOdobrenja"), resultSet.getDouble("iznos"), resultSet.getString("oznakaValute"));
        	nalozi.add(n);
        	NalogBanka nn = new NalogBanka("", resultSet.getString("uplatilac"), resultSet.getString("svrhaUplate"), resultSet.getString("primalac"), resultSet.getDate("datumNaloga"), resultSet.getDate("datumValute"), resultSet.getString("racunUplatioca"), resultSet.getInt("modelZaduzenja"), resultSet.getString("pozivNaBrojZaduzenja"), resultSet.getString("racunPrimaoca"), resultSet.getInt("modelOdobrenja"), resultSet.getString("pozivNaBrojOdobrenja"), resultSet.getDouble("iznos"), resultSet.getString("oznakaValute"), resultSet.getInt("hitno")==1 ? true : false);
        	naloziBanke.add(nn);
        }
        System.out.println("*********COLECTED " + naloziBanke.size() + " ORDERS ********");
        URL wsdlLocation = new URL("http://localhost:8082/nbs-ws/services/CBWS?wsdl");
		QName serviceName = new QName("http://nbs", "CBWSService");
		QName portName = new QName("http://nbs", "CBWSPort");

		Service service = Service.create(wsdlLocation, serviceName);
		
		CBWS cbws = service.getPort(portName, CBWS.class);
		java.util.Date d = new java.util.Date();
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(d);
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(naloziBanke.get(0).getDatumValute());
        XMLGregorianCalendar date3 = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);

        
	//	Date datumValute = naloziBanke.get(0).getDatumValute();
		System.out.println("*************GOING TO SETTLE**********");
		Mt900 mt900 = cbws.settle(nalozi,date3);
		System.out.println("***************FINISHED SETTLING************");
		for (NalogBanka n : naloziBanke){
			c.setTime(n.getDatumNaloga());
			XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

			nbs.data.Mt103 mt103 = new nbs.data.Mt103("", "", "", "", "", n.getUplatilac(), n.getSvrhaUplate(), n.getPrimalac(), date, date2, n.getRacunUplatioca(), n.getModelZaduzenja(), n.getPozivNaBrojZaduzenja(), n.getRacunPrimaoca(), n.getModelOdobrenja(), n.getPozivNaBrojOdobrenja(), n.getIznos(), n.getOznakaValute());
			skiniNovac(mt103);
		}
		emptyNalogBanka();
	}
	
	public ArrayList<izvod.data.Nalog> clear2(String bankCode) throws SQLException, MalformedURLException, DatatypeConfigurationException{
		String query = "select * from nalogbanka where bankCodePrimaoca = ? ";
        preparedStatement = connect.prepareStatement(query);
        preparedStatement.setString(1, bankCode);
        resultSet = preparedStatement.executeQuery();
        
        ArrayList<izvod.data.Nalog> nalozi = new ArrayList<izvod.data.Nalog>();
        while(resultSet.next()){
        	GregorianCalendar c = new GregorianCalendar();
			c.setTime(resultSet.getDate("datumNaloga"));
			XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        	izvod.data.Nalog n = new izvod.data.Nalog(resultSet.getString("idNalogBanka"), resultSet.getString("uplatilac"), resultSet.getString("svrhaUplate"), resultSet.getString("primalac"), date, resultSet.getString("racunUplatioca"), resultSet.getInt("modelZaduzenja"), resultSet.getString("pozivNaBrojZaduzenja"), resultSet.getString("racunPrimaoca"), resultSet.getInt("modelOdobrenja"), resultSet.getString("pozivNaBrojOdobrenja"), resultSet.getDouble("iznos"), resultSet.getString("oznakaValute"));
        	nalozi.add(n);
        }
        return nalozi;
	}
	
	public void dodajNovac(Mt102 mt102, Mt910 mt910) throws DatatypeConfigurationException, SQLException{
		ArrayList<izvod.data.Nalog> nalozi =  mt102.getNalozi();
		for(izvod.data.Nalog n : nalozi){
			GregorianCalendar c = new GregorianCalendar();
			
			java.util.Date date = n.getDatumNaloga().toGregorianCalendar().getTime();
			Date date1 = new Date(date.getTime());
			
			//c.setTime(mt102.getDatumValute());
			//XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

			java.util.Date date2 = mt102.getDatumValute().toGregorianCalendar().getTime();
			Date date22 = new Date(date2.getTime());
			
			izvod.data.Mt103 mt103 = new izvod.data.Mt103(mt102.getIdPoruke(), mt102.getSwiftBankeD(), mt102.getRacunBankeD(), mt102.getSwiftBankeP(), mt102.getRacunBankeP(), n.getUplatilac(), n.getSvrhaUplate(), n.getPrimalac(), date1, date22, n.getRacunUplatioca(), n.getModelZaduzenja(), n.getPozivNaBrojZaduzenja(), n.getRacunPrimaoca(), n.getModelOdobrenja(), n.getPozivNaBrojOdobrenja(), n.getIznos(), n.getOznakaValute());
			finishTransaction(mt103, mt910);
		}
	}
	
	public void emptyNalogBanka(){
		String query = "TRUNCATE TABLE NALOGBANKA";
        try {
			preparedStatement = connect.prepareStatement(query);
	       preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
}
