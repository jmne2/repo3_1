
package nbs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nbs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SettleResponse_QNAME = new QName("http://nbs", "settleResponse");
    private final static QName _Settle_QNAME = new QName("http://nbs", "settle");
    private final static QName _GetNalogResponse_QNAME = new QName("http://nbs", "getNalogResponse");
    private final static QName _SendNalogResponse_QNAME = new QName("http://nbs", "sendNalogResponse");
    private final static QName _SendNalog_QNAME = new QName("http://nbs", "sendNalog");
    private final static QName _GetNalog_QNAME = new QName("http://nbs", "getNalog");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nbs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetNalog }
     * 
     */
    public GetNalog createGetNalog() {
        return new GetNalog();
    }

    /**
     * Create an instance of {@link SendNalogResponse }
     * 
     */
    public SendNalogResponse createSendNalogResponse() {
        return new SendNalogResponse();
    }

    /**
     * Create an instance of {@link GetNalogResponse }
     * 
     */
    public GetNalogResponse createGetNalogResponse() {
        return new GetNalogResponse();
    }

    /**
     * Create an instance of {@link SendNalog }
     * 
     */
    public SendNalog createSendNalog() {
        return new SendNalog();
    }

    /**
     * Create an instance of {@link Settle }
     * 
     */
    public Settle createSettle() {
        return new Settle();
    }

    /**
     * Create an instance of {@link SettleResponse }
     * 
     */
    public SettleResponse createSettleResponse() {
        return new SettleResponse();
    }

    /**
     * Create an instance of {@link ArrayList }
     * 
     */
    public ArrayList createArrayList() {
        return new ArrayList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SettleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://nbs", name = "settleResponse")
    public JAXBElement<SettleResponse> createSettleResponse(SettleResponse value) {
        return new JAXBElement<SettleResponse>(_SettleResponse_QNAME, SettleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Settle }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://nbs", name = "settle")
    public JAXBElement<Settle> createSettle(Settle value) {
        return new JAXBElement<Settle>(_Settle_QNAME, Settle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNalogResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://nbs", name = "getNalogResponse")
    public JAXBElement<GetNalogResponse> createGetNalogResponse(GetNalogResponse value) {
        return new JAXBElement<GetNalogResponse>(_GetNalogResponse_QNAME, GetNalogResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendNalogResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://nbs", name = "sendNalogResponse")
    public JAXBElement<SendNalogResponse> createSendNalogResponse(SendNalogResponse value) {
        return new JAXBElement<SendNalogResponse>(_SendNalogResponse_QNAME, SendNalogResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendNalog }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://nbs", name = "sendNalog")
    public JAXBElement<SendNalog> createSendNalog(SendNalog value) {
        return new JAXBElement<SendNalog>(_SendNalog_QNAME, SendNalog.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNalog }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://nbs", name = "getNalog")
    public JAXBElement<GetNalog> createGetNalog(GetNalog value) {
        return new JAXBElement<GetNalog>(_GetNalog_QNAME, GetNalog.class, null, value);
    }

}
